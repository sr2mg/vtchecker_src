import React from 'react';

import MuteCard from './Mute.js';
import MatterCard from './Matter.jsx';

import Grid from '@material-ui/core/Grid';

//本要素。Matterをまとめて返す。
class MList extends React.Component{
  
     constructor(props){
       super(props);
       this.state=({
         data:"null",
       });
     }

   //最初のロード時に実行
     componentDidMount(){
       const jsonurl ="https://confident-minsky-7427e2.netlify.app/VTuberfeed.json";
   
       return fetch(jsonurl)
         .then((response)=>response.json())
         .then((myJson)=>{
           //日付順にソート
           myJson.sort(function(a,b){return (a.Published<b.Published ?1:-1)});

           //Jsonデータを要素に写す。いつかCardlistItemsとPaperlistItemsをまとめたい
           const CardlistItems = myJson.map((num)=>
             <MatterCard
               key={num.ChID}
               ChID={num.ChID}
               VideoID={num.VideoID}
               VideoURL={"https://www.youtube.com/watch?v="+num.VideoID}
               SmnURL={"https://i.ytimg.com/vi/"+num.VideoID+"/mqdefault.jpg"}
               ChURL={"https://www.youtube.com/channel/"+num.ChID}
               title={num.title}
               ChName={num.author}
               Published={num.Published}
             />
           );

         const MutelistItems = myJson.map((num)=>
         <MuteCard
           key={num.ChID}
           ChID={num.ChID}
           VideoID={num.VideoID}
           VideoURL={"https://www.youtube.com/watch?v="+num.VideoID}
           ChURL={"https://www.youtube.com/channel/"+num.ChID}
           title={num.title}
           ChName={num.author}
           Published={num.published}
         />
       );
           //はやめに直打ちをやめる方法を考える
           this.setState({
             data:myJson,
             CardlistItems:CardlistItems,
             MutelistItems:MutelistItems,
             RecommendID:"YgGHv4rIIZ0",
             RecommendCh:"UC5Am37z4orWOwfPbmy1liyg",
             RecommendTitle:"【VALORANT】初心者Vtuberがヴァイパーで36キル！怒涛の無双【新人Vtuber】",
             RecommendChName:"うにまさ",
             RecommendPublished:"2020/06/22",
           });
         });
     }
   
     render(){//ifで2だったらMutelistItemsを渡す
       switch(this.props.TabState){
         case 0:
           return(
             <Grid container>
             <MatterCard
               key={"1"}
               ChID={this.state.RecommendCh}
               VideoID={this.state.RecommendID}
               VideoURL={"https://www.youtube.com/watch?v="+this.state.RecommendID}
               SmnURL={"https://i.ytimg.com/vi/"+this.state.RecommendID+"/mqdefault.jpg"}
               ChURL={"https://www.youtube.com/channel/"+this.state.RecommendCh}
               title={this.state.RecommendTitle}
               ChName={this.state.RecommendChName}
               Published={this.state.RecommendPublished}
             />
             {this.state.CardlistItems}
             </Grid>
           )
         default:
           return(
             <Grid container>
               {this.state.MutelistItems}
             </Grid>
           )
       }
   }
   }
   export default MList