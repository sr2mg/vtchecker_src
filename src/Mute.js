import React from 'react';
import Cookies from 'js-cookie';
import { useState } from 'react';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const muteStyles = makeStyles({
     root:{
     },
     button:{
     }
   })
   
   function MuteCard(props){
     const classes=muteStyles();
     //ミュート機能。CookieMuteにてCookieでのミュート状況を確認、mutedの初期値に。setMuteを呼び出すとCookie更新とhooks更新
     let CookieMute =Cookies.get(`${props.ChID}`);
     const [muted,setMuted]=useState(CookieMute);
     function setMute(){
       Cookies.set(props.ChID,"false",{"sameSite":"lax"});
       setMuted(("false"),[setMuted]);
     }
       if(muted==="true"){
         return(
           <Card className={classes.root}>
           <Box>
           <Box alignItems="flex-start">{props.ChName}</Box>
           <Box alignItems="flex-end">
             <Button className={classes.button} variant="outlined" color="primary" onClick={()=>setMute()}>
             ミュート解除
             </Button>
             </Box>
           </Box>
           </Card>
         )
       }else{
         return(
           <div></div>
         )
       }
   }

export default MuteCard