import React from 'react';

import PropTypes from 'prop-types';



import MList from './MList.js';
import Discription from './Discription.js';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';


function App() {
//test
const [value, setValue] = React.useState(0);
const handleChange = (event, newValue) => {
  setValue(newValue);
};

return (
    <div className="App">
        <AppBar color="primary">
          <Toolbar>
            <Typography variant="h6">VTuberChecker</Typography>
            <Tabs value={value} onChange={handleChange}>
              <Tab label ="Design1" />
              <Tab label ="Settings"/>
              <Tab label ="About"/>
            </Tabs>
          </Toolbar>
        </AppBar>

        <Toolbar />
          <Grid container>
              <Grid item xs />
              <Grid item xs={9}>
                <Paper>
                  <TabPanel value={value} index={0}>
                    <MList TabState={0}/>
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <Typography margin={5}>
                      ミュートリスト
                    </Typography>
                    <MList TabState={1}/>
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <Discription />
                  </TabPanel>
                </Paper>
              </Grid>
              <Grid item xs />
          </Grid>

    </div>
  );
}

//*ここから導入テスト
function TabPanel(props){
  const{children,value,index}=props;//propsから受け取ったものをconstに入れてる
  return (
    <div
      hidden={value !== index}
      id={`tabpanel-${index}`}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}
//proptypesは型チェックを行うもの。nodeというのはrenderできるかの判定。
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

export default App;