import React from 'react';
import Cookies from 'js-cookie';
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Link from '@material-ui/core/Link';
import Grow from '@material-ui/core/Grow';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Typography from '@material-ui/core/Typography';


//UTCからJSTに変える
function UTCtoJST(props){
  let UTCtime =new Date(props)
  UTCtime.setTime(UTCtime.getTime());
  return UTCtime.toLocaleString();
}
//Cardのレイアウト
const cardStyles= makeStyles({
  root:{
    margin:5,
    padding:2,
    maxWidth:310,
  },
  media:{
    paddingTop:"56.25%"
  },
});

//hooksを用いた。ミュートか否かはmutedにstateとして保存。初回はCookieを参照。
function MatterCard(props){
     //ミュート機能。CookieMuteにてCookieでのミュート状況を確認、mutedの初期値に。setMuteを呼び出すとCookie更新とhooks更新
     let CookieMute =Cookies.get(`${props.ChID}`);
     const [muted,setMuted]=useState(CookieMute);
     function setMute(){
       Cookies.set(props.ChID,"true",{"sameSite":"lax"});
       setMuted(("true"),[setMuted]);
     }

     //メニュー表示
     const[anchorEl,setAnchorEl] = React.useState(null);
     const closeMenu=()=>{
       setAnchorEl(null);
     };
     const openMenu=(event)=>{
       setAnchorEl(event.currentTarget);
     };
   
     const classes = cardStyles();
     //ミュート時(true)非表示
     if(muted==="true"){
       return(<div></div>)
     }else{
     return(
             <Grow in>
             <Card className={classes.root}>
             <CardHeader 
               title={props.ChName}
               action={
                 <IconButton>
                 <MoreVertIcon onClick={openMenu}/>
                 </IconButton>
               }
             />
             <Menu
                       id="simple-menu"
                       anchorEl={anchorEl}
                       keepMounted
                       open={Boolean(anchorEl)}
                       onClose={closeMenu}
             >
               <MenuItem onClick={setMute}>ミュート</MenuItem>
             </Menu>
             <Link href={props.VideoURL} target="_blank" rel="noopener">
               <CardActionArea>
                 <CardMedia
                   className={classes.media}
                   image={props.SmnURL}
                   title={props.title}
                 />
                 <CardContent>
                   <Typography variant="body1" color="textSecondary" component="p">
                     {props.title}
                   </Typography>
                   <Typography variant ="body2" color="textSecondary">
                     {UTCtoJST(props.Published)}
                   </Typography>
                 </CardContent>
               </CardActionArea>
               </Link>
             </Card>
             </Grow>
     );
    }
   }

   export default MatterCard