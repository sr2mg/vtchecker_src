import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

function Discription(){
     return (
         <Grid>
         <Typography variant ="body2" color="textSecondary">
         自分のオススメVTuberの最新動画をまとめたサイトです。更新は1時間に一回です。
         </Typography>
         <Typography variant ="body2" color="textSecondary">
         アップロードされた日付順で上から並ぶようになっています。ミュートにしたいVTuberがいた場合は、カードの右上からミュートを選んでください。
         解除は設定から行えます。
         </Typography>
         <Typography variant ="body2" color="textSecondary">
         このサイトに掲載する際、掲載許可は取っています。
         何かありましたら<a href="https://twitter.com/sald_ra">Twitter</a>までご連絡ください。
         </Typography>
         </Grid>    
     )
   }

export default  Discription